package BIT;

public class Pair {
    private Long x;
    private Long y;

    public Pair(int x, int y) {
        this.x = Long.valueOf(x);
        this.y = Long.valueOf(y);
    }

    public Pair(long x, long y) {
        this.x = x;
        this.y = y;
    }

    public Long getX() {
        return this.x;
    }

    public Long getY() {
        return this.y;
    }

    public void addX(int x) {
        this.x += x;
    }

    public void addY(int y) {
        this.y += y;
    }

    public void addX(long x) {
        this.x += x;
    }

    public void addY(long y) {
        this.y += y;
    }
}
