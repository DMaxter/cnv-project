package BIT;

import java.io.File;
import java.util.Enumeration;
import java.util.Vector;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import BIT.highBIT.*;

public class Statistics {
    // Pair(instructions, memory)
    private static Map<Long, Pair> metrics = new ConcurrentHashMap<Long, Pair>();

    public static void countInstr(int blocksize) {
        Long id = Thread.currentThread().getId();
        Pair pair = metrics.get(id);

        if (pair == null) {
            metrics.put(id, new Pair(blocksize, 0));
        } else {
            pair.addX(blocksize);
        }
    }

    public static void countAlloc(int allocs) {
        Long id = Thread.currentThread().getId();
        Pair pair = metrics.get(id);

        if (pair == null) {
            metrics.put(id, new Pair(0, 1));
        } else {
            pair.addY(1);
        }
    }

    public static long getAllInstructions() {
        Long total = 0L;
        for(Long id: metrics.keySet()) {
            total += metrics.get(id).getX();
        }

        return total;
    }

    public static Pair getStats(Long id) {
        return metrics.remove(id);
    }

    public static void doInstr(File indir, File outdir) {
        String[] fileList = indir.list();

        for(String file: fileList) {
            if(file.endsWith(".class")) {
                String infile = indir.getAbsolutePath() + System.getProperty("file.separator") + file;
                String outfile = outdir.getAbsolutePath() + System.getProperty("file.separator") + file;

                ClassInfo ci = new ClassInfo(infile);

                for(Enumeration eR = ci.getRoutines().elements(); eR.hasMoreElements(); ) {
                    Routine routine = (Routine) eR.nextElement();
                    for(Enumeration eB = routine.getBasicBlocks().elements(); eB.hasMoreElements();) {
                        BasicBlock block = (BasicBlock) eB.nextElement();
                        block.addBefore("BIT/Statistics", "countInstr", block.size());
                    }
                    for (Enumeration eI = routine.getInstructionArray().elements(); eI.hasMoreElements(); ) {
                        Instruction instr = (Instruction) eI.nextElement();
                        int opcode = instr.getOpcode();

                        if ((opcode == InstructionTable.NEW) ||
                            (opcode == InstructionTable.newarray) ||
                            (opcode == InstructionTable.anewarray) ||
                            (opcode == InstructionTable.multianewarray)) {
                            instr.addBefore("BIT/Statistics", "countAlloc", 1);
                        }
                    }
                }

                ci.write(outfile);
            }
        }
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Usage: java %s <input dir> <output dir>");
            return;
        }

        File indir = new File(args[0]);
        File outdir = new File(args[1]);

        if (!(indir.isDirectory() && outdir.isDirectory())) {
            System.out.println("Parameters are not directories");
            return;
        }

        doInstr(indir, outdir);
    }
}
