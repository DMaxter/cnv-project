#!/bin/bash

curpath=$PWD
export CLASSPATH="$CLASSPATH:$curpath/BIT:$curpath/AWS-SDK/lib/aws-java-sdk-1.11.1030.jar:$curpath/AWS-SDK/third-party/lib/*:$curpath"
export _JAVA_OPTIONS="-XX:-UseSplitVerifier "$_JAVA_OPTIONS
