#!/bin/bash

# Install Java
sudo yum -y update && sudo yum -y install java-1.7.0-openjdk-devel.x86_64

# Setup webserver on startup
echo "cd /home/ec2-user
source ./config.sh
javac pt/ulisboa/tecnico/cnv/server/WebServer.java
sudo -u ec2-user -E java pt.ulisboa.tecnico.cnv.server.WebServer -address 0.0.0.0" | sudo tee -a /etc/rc.local
sudo chmod +x /etc/rc.d/rc.local

# Run webserver
sudo /etc/rc.local
