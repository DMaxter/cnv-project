
# CNV 2021

  

Before compiling/running, execute `source config.sh`

  

## Compiling and running the instrumentation tool

  

```sh

javac BIT/Statistics.java

java BIT.Statistics pt/ulisboa/tecnico/cnv/backup pt/ulisboa/tecnico/cnv/solver

```

  

## Compiling and running the WebServer

  

```sh

javac pt/ulisboa/tecnico/cnv/server/WebServer.java

java pt.ulisboa.tecnico.cnv.server.WebServer

```

  

# Deployment

The web server will be deployed with AWS. To do so either create or login into an AWS account, all resources in this guide are of the free-tier so a free-tier account is sufficient. After login, select the **US East North Virginia** region available in free-tier.

## Launch Instance

Launch Instance with the following characteristics:
	

 1. Select the **Amazon Linux 2 AMI (HVM), SSD Volume Type** AMI
 2. Select the **t2.micro** instance type and click Next
 3. Select a network, for example **us-east-1a** available in free-tier, activate the monitoring feature (Enable CloudWatch detailed monitoring) and leave the rest to default. Click next.
 4. Maintain the default size of 8 GiB and click Next
 5. No need to add any tags so click Next without adding tags
 6. Create a new security group that accepts type **SSH** through **TCP** protocol though port **22** from any source (**0.0.0.0/0**) and do the same for **HTTP** on port **80** and a **custom TCP** rule on port **8000**. All accept from every source through the TCP protocol. Choose a name for the security group, for example **CNV-Sec-G38** and click Next
 7. Review if all configurations are correct. Notice the warning that the security group is open to the world. This is normal as we accept requests from all sources in the rules created in the previous step. Click Launch, this is now launch a single instance with the configurations chosen before
 8. To have remote access to the instance we must now create a security key pair. Insert a name for the keypair, for example **CNV-G38** and click Download Key Pair. Next click Launch instances to finish the process.

You can now verify the sate of the instance by clicking View Instances, that will lead to the instance menu. Wait for the instance to go from Pending to Running and for the status checks to finalize.  This might take a couple of minutes.

## Connect to the Instance

Now we will connect to the instance from our system. For consistency we will use the virtual machine presented by the course.

 1. Create a folder named **CNV-G38** inside the shared folder of the VM machine
 2. Copy the **CNV-G38.pem** to the **CNV-G38**  folder
 3. Change the permissions of the key pair file with  ```sudo chmod 0500 CNV-G38.pem``` so it can only be read and executed by the Superuser
 4. Get the IP address and default user of the instance from the instance menu of the AWS browser. Select the instance previously created and click Connect on the top the page. It will present the user name and public IP address of the instance
 5. Connect to the instance with ```ssh - i CNV-G38.pem -l <default_user> <ip_address>```. Choose yes when prompted

## Install Packages

To run the webserver we need to check for updates and install java packages. To do so run the following commands on the shell that is connect to the instance:

```sh
sudo yum update

sudo yum search jdk

sudo yum install java-1.7.0-openjdk-devel.x86_64
```
Say yes when prompted. To verify the correct installation run 
```sh
java -version

javac -version
```
If version 1.7 is recognized than the packages were successfully installed

## Run the WebServer

Now we will run the Webserver on the instance we have created.

 1. Pass the Webserver code to the instance, for example via git or google drive
 2. Compile the code with ```javac pt/ulisboa/tecnico/cnv/server/WebServer.java```
 3. Make the Webserver start at launch by editing the /etc/rc.local file with sudo permisions and add the lines
```sh
cd /home/ec2-user
source ./config.sh
java pt.ulisboa.tecnico.cnv.server.WebServer -address 0.0.0.0
```
 4. Set the permissions of the file with ```sudo chmod +x /etc/rc.d/rc.local```
 5. Stop and restart the instance to confirm that the Webserver starts running


## Create the Image

 1. Go to the instance menu
 2. Select the instance created previously, right-click and under Image and Templates, click create Image
 3. Select a name, for example **CNV-Image-G38**
 4. Click Create Image
 5. After the image is created we can now terminate the instance

## Set Load Balancer


We will now set the load balancer of this application. 

 1. Select the Load Balancers menu under Load Balancing on the left side of the AWS browser.
 2. Click Create Load Balancer
 3. Select Classic Load Balancer
 4. Select a name for the load balancer, for example **CNV-Load-Balancer-G38**, select Load Balancer Protocol as **HTTP** with load Balancer Port **80**, instance Protocol **HTTP** and Instance port **8000** (this is where the webserver receives requests). Lastly enable advance VPC configuration and select subnet **us-east-1a** to run the instances of this load balancer. Click Next
 5. Create a new security group named **CNC-Load-Balancer-G38** with a rule of type **Custom TCP** with protocol **TCP** on port **80** from any source(**0.0.0.0/0**). Click Next
 6. Click Next
 7. Configure the Health Check with Ping Protocol **HTTP**, Ping Port **8000** and Ping Path **/test**. Click Next
 8. Ignore instances and Click Next
 9. Ignore tags and click Review and Create
 10. Review the configuration and click Create

## Create Launch Configuration

 1. Select the Launch Configurations menu under Auto-Scaling on the left side of the AWS browser.
 2. Click Create Launch Configuration
 3. Select a name, for example **CNV-Launch-Config-G38**, select the Image previously created (**CNV-Image-G38**) and select instance type **t2.micro**. Enable Monitoring (Enable Ec2 instance detailed monitoring within CloudWatch). Select the security group and key pair created when we created the first instance (**CNV-Sec-G38** and **CNV-G38** respectively). Scroll to the end of the page and click Create launch Configuration 

## Create Auto-Scaling Group

 1. Select the Auto Scaling Groups menu under Auto-Scaling on the left side of the AWS browser.
 2. Click Create Auto Scaling Group
 3. Select a name, for example **CNV-Scaling-G38**, click in Switch to launch configuration and select the previously created launch configuration (**CNV-Launch-Config-G38**). Click Next
 4. Select the **us-east-1a** subnet and click Next
 5. Select Attach to an existing load balancer, choose from classic load balancer and select the previously created load balancer **CNC-Load-Balancer-G38**. Check ELB health checks, with a grace period of 60s and lastly enable monitoring (Enable group metric collection within CloudWatch). Click Next
 6. Select **3** as maximum capacity and choose **None** as the scaling policy. Click Next
 7. Ignore notifications and click Next
 8. Ignore tags and click Next
 9. Review the configuration and click Create Auto Scaling Group

## Define CloudWatch Alarms

 1. Select the CloudWatch under Management & Governance on the services dropdown
 2. Click the In alarm menu under Alarms on the left side of the AWS browser
 3. Click Create alarm
 4. Click Select Metric
 5. Search for CPU and select **EC2 > By Auto Scaling Group** and then the metric of **CPU Utilization**. Click Select Metric. Change the period to **1 minute** and change the condition to **higher than50**. Click Next
 6. Click Remove in the Notification section and click Next
 7. Select a name for the alarm, for example **High-CPU-Util-G38** and click Next
 8. Review the configurations and click Create Alarm
 9. Repeat the same configuration for a a condition with **lower than 20** named **Low-CPU-Util-G38**

## Create Scaling Policies 

 1. Select the Auto Scaling Groups menu under Auto-Scaling on the left side of the AWS browser.
 2. Select the auto scaling group, go to the automatic scaling and click add Policy
 3. Select Step Scaling, give it a name, for example **Increase-Group-Size-G38**, select the  **High-CPU-Util-G38** alarm and take the action **Add 1 unit**
 4. Repeat the configuration for the condition the **Low-CPU-Util-G38** alarm with the action **Remove 1 unit** and name it **Decrease-Group-Size-G38**

## Configuring the Load Balancer
 1. Select the Load Balancers menu under Load Balancing on the left side of the AWS browser.
 2. Select the **CNV-Load-Balancer-G38**
 3. In the **Description** tab, on the **Attributes** section, click on **Edit idle timeout**
 4. Insert **360** (so that connections which last more than 6mins get dropped) and click **Save**

## Configuring the AutoScaler
 1. Select the Auto Scaling Groups menu under Auto-Scaling on the left side of the AWS browser.
 2. Select the group **CNV-Scaling-G38** and on the **Details** tab go to the **Advanced Configuration** section and click **Edit**
 3. Set the default cooldown to **150** seconds and click **Update**
