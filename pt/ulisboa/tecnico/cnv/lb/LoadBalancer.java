package pt.ulisboa.tecnico.cnv.lb;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.cloudwatch.model.Metric;
import com.amazonaws.services.cloudwatch.model.MetricDataQuery;
import com.amazonaws.services.cloudwatch.model.MetricDataResult;
import com.amazonaws.services.cloudwatch.model.MetricStat;
import com.amazonaws.services.cloudwatch.model.ScanBy;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.util.TableUtils;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import java.lang.Thread;
import java.net.InetSocketAddress;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;

class LoadBalancer {
    public static final int PENDING_CONNECTIONS = 10;
    public static final String REGION = "us-east-1";
    public static final String SEC_GROUP_INSTANCE_NAME = "G38_SEC_GROUP3";
    public static final String SEC_GROUP_INSTANCE_DESC = "G38_SEC_GROUP3";
    public static final String AWS_AMI_ID = "ami-05b18bad2392838c5";
    public static final String INSTANCE_TYPE = "t2.micro";
    public static final String TABLE_NAME_DETAILS = "RadarCloud_Metrics_Details";
    public static final String TABLE_NAME_AVERAGE = "RadarCloud_Metrics_Average";
    public static final String CLOUDWATCH_NAMESPACE = "AWS/EC2";
    public static final String METRIC_CPU_UTILIZATION = "CPUUtilization";
    public static final int MIN_INSTANCES = 1;
    public static final int MAX_INSTANCES = 3;
    public static final int MAX_REQUESTS = 5;
    public static final double UNKNOWN_WEIGHT = 16000;
    public static final double MAX_WEIGHT = 75_000_000_000.0;
    public static final double WEIGHT_THRESHOLD = 0.85 * MAX_WEIGHT;
    public static final int POLL_TIME = 60;
    public static final int SLEEP_TIME = 20;

    // TODO: QUEREMOS O 20 B-)

    static AmazonDynamoDB dynamoDB;
    static AmazonEC2 ec2;
    static AmazonCloudWatch cw;
    static int curIter = 0;
    static Object InstLock = new Object();
    static Object IPLock = new Object();
    static Object ReqLock = new Object();

    // Thread to get instance IPs
    static Thread IPThread =
            new Thread(new Runnable() {
                // Constantly check if there are any instances waiting for IP
                @Override
                public void run() {
                    while (true) {
                        // When there are no instances waiting for IP, block
                        int size = 0;
                        try {
                            synchronized (IPLock) {
                                IPLock.wait();
                                size = noIP.size();
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        // Polling while trying to get IP
                        while (size != 0) {
                            List<String> toRemove = new ArrayList<>();
                            for (String instance : noIP) {
                                try {
                                    DescribeInstancesRequest describeRequest = new DescribeInstancesRequest().withInstanceIds(instance);

                                    DescribeInstancesResult result = ec2.describeInstances(describeRequest);

                                    for (Reservation reservation : result.getReservations()) {
                                        for (Instance i : reservation.getInstances()) {
                                            if (i.getState().getName().equals("running")) {
                                                System.out.println("Instance " + instance + ": RUNNING");
                                                instanceIds.put(instance, new InstanceInfo(instance, i.getPublicIpAddress()));
                                                toRemove.add(instance);

                                                // After instance is running, wake all requests
                                                synchronized (ReqLock) {
                                                    ReqLock.notifyAll();
                                                }
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            try {
                                Thread.sleep(SLEEP_TIME * 1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            synchronized (IPLock) {
                                noIP.removeAll(toRemove);
                                size = noIP.size();
                            }
                        }
                    }

                }
            });


    static Map<String, InstanceInfo> instanceIds = new ConcurrentHashMap<>();
    static Map<String, Thread> threadQueue = new ConcurrentHashMap<>();
    static List<String> noIP = new CopyOnWriteArrayList<>();

    public static void init() throws AmazonClientException {
        ProfileCredentialsProvider credentialsProvider = new ProfileCredentialsProvider();
        try {
            credentialsProvider.getCredentials();

            dynamoDB = AmazonDynamoDBClientBuilder.standard()
                    .withCredentials(credentialsProvider)
                    .withRegion(REGION)
                    .build();

            cw = AmazonCloudWatchClientBuilder.standard()
                    .withCredentials(credentialsProvider)
                    .withRegion(REGION)
                    .build();

            // Create a table with a primary hash key named 'name', which holds a string
            CreateTableRequest createTableRequest = new CreateTableRequest().withTableName(TABLE_NAME_DETAILS)
                    .withKeySchema(new KeySchemaElement().withAttributeName("request").withKeyType(KeyType.HASH))
                    .withAttributeDefinitions(new AttributeDefinition().withAttributeName("request").withAttributeType(ScalarAttributeType.S))
                    .withProvisionedThroughput(new ProvisionedThroughput().withReadCapacityUnits(50L).withWriteCapacityUnits(50L));

            // Create a table with a primary hash key named 'name', which holds a string
            CreateTableRequest createTableRequest2 = new CreateTableRequest().withTableName(TABLE_NAME_AVERAGE)
                    .withKeySchema(new KeySchemaElement().withAttributeName("algorithm").withKeyType(KeyType.HASH))
                    .withAttributeDefinitions(new AttributeDefinition().withAttributeName("algorithm").withAttributeType(ScalarAttributeType.S))
                    .withProvisionedThroughput(new ProvisionedThroughput().withReadCapacityUnits(50L).withWriteCapacityUnits(50L));


            // Create table if it does not exist yet
            TableUtils.createTableIfNotExists(dynamoDB, createTableRequest);
            TableUtils.createTableIfNotExists(dynamoDB, createTableRequest2);

            // wait for the table to move into ACTIVE state
            TableUtils.waitUntilActive(dynamoDB, TABLE_NAME_DETAILS);
            TableUtils.waitUntilActive(dynamoDB, TABLE_NAME_AVERAGE);

            ec2 = AmazonEC2ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentialsProvider.getCredentials()))
                    .withRegion(REGION)
                    .build();

            // Create Instances' Security Group
            CreateSecurityGroupRequest securityGroupRequest = new CreateSecurityGroupRequest(SEC_GROUP_INSTANCE_NAME, SEC_GROUP_INSTANCE_DESC);
            CreateSecurityGroupResult result = ec2.createSecurityGroup(securityGroupRequest);

            String ipAddr = "0.0.0.0/0";

            List<String> ipRanges = Collections.singletonList(ipAddr);
            List<IpPermission> ipPermissions = new ArrayList<IpPermission>();

            // SSH
            IpPermission ipPermission = new IpPermission()
                    .withIpProtocol("tcp")
                    .withFromPort(new Integer(22))
                    .withToPort(new Integer(22))
                    .withIpRanges(ipRanges);

            ipPermissions.add(ipPermission);

            // WebServer
            ipPermission = new IpPermission()
                    .withIpProtocol("tcp")
                    .withFromPort(new Integer(8000))
                    .withToPort(new Integer(8000))
                    .withIpRanges(ipRanges);

            ipPermissions.add(ipPermission);

            AuthorizeSecurityGroupIngressRequest ingressRequest = new AuthorizeSecurityGroupIngressRequest(SEC_GROUP_INSTANCE_NAME, ipPermissions);
            ec2.authorizeSecurityGroupIngress(ingressRequest);

            Scaler.init();
        } catch (AmazonServiceException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            throw new AmazonClientException(e);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        IPThread.start();

        init();

        final HttpServer server = HttpServer.create(new InetSocketAddress("0.0.0.0", 8000), 10);

        server.createContext("/scan", new Balancer());
        server.setExecutor(Executors.newFixedThreadPool(50));
        server.start();

        Scaler.eval();
    }

    static class Balancer implements HttpHandler {
        @Override
        public void handle(final HttpExchange t) throws IOException {
            // Get the query
            final String query = t.getRequestURI().getQuery();

            // Break it down into String[].
            final String[] params = query.split("&");

            // Store as if it was a direct call to SolverMain
            final ArrayList<String> newArgs = new ArrayList<>();
            Map<String, String> reqArgs = new HashMap<>();
            for (final String p : params) {
                final String[] splitParam = p.split("=");

                if (splitParam[0].equals("s") || splitParam[0].equals("h") || splitParam[0].equals("w") || splitParam[0].equals("x0") || splitParam[0].equals("x1") || splitParam[0].equals("y0") || splitParam[0].equals("y1")) {
                    reqArgs.put(splitParam[0], splitParam[1]);
                }

                newArgs.add("-" + splitParam[0]);
                newArgs.add(splitParam[1]);

            }

            String x0 = reqArgs.get("x0");
            if (x0 == null) {
                x0 = "0";
            }
            String x1 = reqArgs.get("x1");
            if (x1 == null) {
                x1 = Integer.toString(Integer.parseInt(reqArgs.get("w")) - 1);
            }
            String y0 = reqArgs.get("y0");
            if (y0 == null) {
                y0 = "0";
            }
            String y1 = reqArgs.get("y1");
            if (y1 == null) {
                y1 = Integer.toString(Integer.parseInt(reqArgs.get("h")) - 1);
            }


            Map<String, AttributeValue> key = new HashMap<>();
            key.put("algorithm", new AttributeValue(reqArgs.get("s")));

            InstanceInfo chosen = null;

            // Calculate current request weight based on stored information
            // Weight = metric x height x width
            int width = Integer.parseInt(x1) - Integer.parseInt(x0);
            int height = Integer.parseInt(y1) - Integer.parseInt(y0);

            // In case there are no metrics for current request
            GetItemRequest request = new GetItemRequest(TABLE_NAME_AVERAGE, key);

            //Contains current stored metrics about the specific type of request
            Map<String, AttributeValue> response = dynamoDB.getItem(request).getItem();

            double estimated = (response == null) ? UNKNOWN_WEIGHT : Double.parseDouble(response.get("average").getN().toString());
            double current_weight = estimated * width * height;

            // Keep trying to get an instance assigned
            while (chosen == null) {
                synchronized (InstLock) {
                    final List<InstanceInfo> instances = new ArrayList(instanceIds.values());
                    for (InstanceInfo info : instances) {
                        if (chosen == null) {
                            if (info.getWeight() + current_weight <= MAX_WEIGHT && healthCheck(info.getIP())) {
                                chosen = info;
                            }
                        } else if (chosen.getWeight() > info.getWeight() && info.getWeight() + current_weight <= MAX_WEIGHT && healthCheck(info.getIP())) {
                            chosen = info;
                        }
                    }

                    if (chosen != null) {
                        chosen.incWeight(current_weight);
                        System.out.println("Redirecting request to " + chosen.getIP());
                        break;
                    } else {
                        System.out.println("No resources available. Waiting " + POLL_TIME * 0.5 + " seconds");
                    }
                }

                // Put request in queue
                synchronized (ReqLock) {
                    try {
                        ReqLock.wait(POLL_TIME * 500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            // Redirect request
            try {
                URL url = new URL("http://" + chosen.getIP() + ":8000/scan?" + query);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(3600000);
                connection.setReadTimeout(3600000);
                connection.setRequestMethod("GET");

                Map<String, List<String>> headers = connection.getHeaderFields();
                final Headers hdrs = t.getResponseHeaders();
                long length = 0;
                for (String header : headers.keySet()) {
                    if (header == null) {
                        continue;
                    }

                    if (header.equals("Content-Length")) {
                        length = Long.parseLong(headers.get(header).get(0));
                    }
                }

                t.sendResponseHeaders(connection.getResponseCode(), length);

                byte[] buffer = new byte[8 * 1024];
                int len;
                final InputStream in = connection.getInputStream();
                final OutputStream out = t.getResponseBody();
                while ((len = in.read(buffer)) > 0) {
                    out.write(buffer, 0, len);
                }

                connection.disconnect();
                out.close();
            } catch (Exception e) {
                t.close();
            }

            chosen.decWeight(current_weight);

            synchronized (ReqLock) {
                ReqLock.notifyAll();
            }

            // Check if instance can be removed
            if (threadQueue.containsKey(chosen.name) && chosen.getWeight() < 1) {
                synchronized (threadQueue.get(chosen.name)) {
                    threadQueue.get(chosen.name).notify();
                }
            }
        }
    }

    static boolean healthCheck(String uri) {
        try {
            URL url = new URL("http://" + uri + ":8000/test");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            return connection.getResponseCode() == HttpURLConnection.HTTP_OK;
        } catch (Exception e) {
            return false;
        }
    }

    static class Scaler {
        private static int n_instances = 0;

        public static void init() {
            n_instances++;

            RunInstancesRequest requestRequest = new RunInstancesRequest();

            requestRequest.setImageId(AWS_AMI_ID);
            requestRequest.setInstanceType(INSTANCE_TYPE);
            requestRequest.setMinCount(Integer.valueOf(1));
            requestRequest.setMaxCount(Integer.valueOf(1));
            requestRequest.setMonitoring(true);

            // Add the security group to the request.
            ArrayList<String> securityGroups = new ArrayList<String>();
            securityGroups.add(SEC_GROUP_INSTANCE_NAME);
            requestRequest.setSecurityGroups(securityGroups);

            // Launch the instance.
            RunInstancesResult runResult = ec2.runInstances(requestRequest);

            // Add the instance id into the instance id list, so we can potentially later
            // terminate that list.
            String name = null;
            for (Instance instance : runResult.getReservation().getInstances()) {
                name = instance.getInstanceId();
                System.out.println("Launched Instance: " + name);

                synchronized (IPLock) {
                    noIP.add(name);
                    IPLock.notify();
                }
            }
        }

        public static void eval() throws InterruptedException {
            while (true) {
                boolean allBurning = true;
                boolean firstLowCpu = true;
                double cpu = 0;
                try {
                    // Set CloudWatch
                    Dimension instanceDimension = new Dimension();
                    instanceDimension.setName("InstanceId");
                    List<Dimension> dims = new ArrayList<Dimension>();
                    dims.add(instanceDimension);
                    for (String instance : instanceIds.keySet()) {
                        instanceDimension.setValue(instance);

                        GetMetricStatisticsRequest request = new GetMetricStatisticsRequest()
                                .withStartTime(new Date(new Date().getTime() - 120 * 1000))
                                .withNamespace("AWS/EC2")
                                .withPeriod(120)
                                .withMetricName("CPUUtilization")
                                .withStatistics("Average")
                                .withDimensions(instanceDimension)
                                .withEndTime(new Date());
                        GetMetricStatisticsResult getMetricStatisticsResult = cw.getMetricStatistics(request);

                        List<Datapoint> datapoints = getMetricStatisticsResult.getDatapoints();

                        // In case instance just launched
                        if (datapoints.isEmpty()) {
                            System.out.println("No CPU info for instance " + instance);
                            if (instanceIds.get(instance).getWeight() < WEIGHT_THRESHOLD) {
                                allBurning = false;
                            }
                        }

                        for (Datapoint dp : datapoints) {
                            System.out.println("CPU utilization for instance " + instance + " = " + dp.getAverage());
                            cpu = dp.getAverage();
                            if (cpu < 10) {
                                if (instanceIds.get(instance).getWeight() < WEIGHT_THRESHOLD) {
                                    allBurning = false;
                                }

                                if (firstLowCpu) {
                                    firstLowCpu = false;
                                    continue;
                                }
                                final String toDelete = instance;
                                Thread thread = new Thread() {
                                    public void run() {
                                        try {
                                            removeInstance(toDelete);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                };
                                thread.start();

                            } else if (cpu < 80) {
                                if (instanceIds.get(instance).getWeight() < WEIGHT_THRESHOLD) {
                                    allBurning = false;
                                }
                            }
                        }

                        System.out.println("Instance Info - " + instanceIds.get(instance));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (allBurning && n_instances < MAX_INSTANCES && cpu != 0) {
                    init();
                }

                Thread.sleep(60000);
            }

        }

        public static void removeInstance(String instance) throws InterruptedException {
            // In case only one instance up
            if (instanceIds.keySet().size() == MIN_INSTANCES) {
                System.out.println("Can't remove instance, minimum reached (" + MIN_INSTANCES + ")");
                return;
            }

            System.out.println("Removing instance " + instance);

            try {
                // Remove from LB list
                InstanceInfo removing = instanceIds.remove(instance);

                // Wait for completion
                if (removing.getWeight() != 0) {
                    synchronized (Thread.currentThread()) {
                        threadQueue.put(instance, Thread.currentThread());
                        Thread.currentThread().wait();
                    }
                }

                // Terminate instances
                TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest();
                terminateRequest.withInstanceIds(instance);
                ec2.terminateInstances(terminateRequest);

                System.out.println("Instance " + instance + " successfully removed");
            } catch (AmazonServiceException e) {
                System.out.println("Error terminating instance " + instance);
            }
        }
    }

    static class InstanceInfo {
        private final String name;
        private final String IP;
        private double weight = 0;

        public InstanceInfo(String name, String IP) {
            this.name = name;
            this.IP = IP;
        }

        public String getIP() {
            return IP;
        }

        public String getName() {
            return name;
        }

        public synchronized void incWeight(double weight) {
            this.weight += weight;
        }

        public synchronized void decWeight(double weight) {
            this.weight -= weight;
        }

        public double getWeight() {
            return this.weight;
        }

        @Override
        public String toString() {
            return this.name + " IP: " + this.IP + " W: " + this.weight;
        }
    }
}
