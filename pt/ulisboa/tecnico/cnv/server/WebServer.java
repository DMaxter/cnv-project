package pt.ulisboa.tecnico.cnv.server;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.util.TableUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import java.util.concurrent.Executors;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import pt.ulisboa.tecnico.cnv.solver.Solver;
import pt.ulisboa.tecnico.cnv.solver.SolverFactory;

import javax.imageio.ImageIO;

import BIT.Statistics;
import BIT.Pair;

public class WebServer {

    static ServerArgumentParser sap = null;
    static AmazonDynamoDB dynamoDB;
    static final String tableNameDetails = "RadarCloud_Metrics_Details";
    static final String tableNameAverage = "RadarCloud_Metrics_Average";


    public static void init() throws AmazonClientException {
        ProfileCredentialsProvider credentialsProvider = new ProfileCredentialsProvider();
        try {
            credentialsProvider.getCredentials();

            dynamoDB = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(credentialsProvider)
                .withRegion("us-east-1")
                .build();

        } catch (Exception e) {
            throw new AmazonClientException(e);
        }
    }

    public static void main(final String[] args) throws Exception {
        init();

        try {
            // Get user-provided flags.
            WebServer.sap = new ServerArgumentParser(args);
        }
        catch(Exception e) {
            System.out.println(e);
            return;
        }

        System.out.println("> Finished parsing Server args.");

        final HttpServer server = HttpServer.create(new InetSocketAddress(WebServer.sap.getServerAddress(), WebServer.sap.getServerPort()), 0);

        server.createContext("/scan", new SolverHandler());
        server.createContext("/test", new Tester());
        server.createContext("/metrics", new Metrics());

        // be aware! infinite pool of threads!
        server.setExecutor(Executors.newCachedThreadPool());
        server.start();

        System.out.println(server.getAddress().toString());
    }

    static class Tester implements HttpHandler {
        @Override
        public void handle(final HttpExchange t) throws IOException {
            t.sendResponseHeaders(200, 0);
            t.close();
        }
    }

    static class Metrics implements HttpHandler {
        @Override
        public void handle(final HttpExchange t) throws IOException {
            String response = Long.toString(Statistics.getAllInstructions());
            t.sendResponseHeaders(200, response.length());
            t.getResponseBody().write(response.getBytes());
            t.close();
        }
    }

    static class SolverHandler implements HttpHandler {
        @Override
        public void handle(final HttpExchange t) throws IOException {

            // Get the query.
            final String query = t.getRequestURI().getQuery();

            final Long id = Thread.currentThread().getId();

            System.out.println("> Query:\t" + query);

            // Break it down into String[].
            final String[] params = query.split("&");

            // Store as if it was a direct call to SolverMain.
            final ArrayList<String> newArgs = new ArrayList<>();
            Map<String, String> reqArgs = new HashMap<>();
            for (final String p : params) {
                final String[] splitParam = p.split("=");

                if(splitParam[0].equals("i")) {
                    splitParam[1] = WebServer.sap.getMapsDirectory() + "/" + splitParam[1];
                } else if (splitParam[0].equals("s") || splitParam[0].equals("h") || splitParam[0].equals("w") || splitParam[0].equals("x0") || splitParam[0].equals("x1") || splitParam[0].equals("y0") || splitParam[0].equals("y1")) {
                    reqArgs.put(splitParam[0], splitParam[1]);
                }

                newArgs.add("-" + splitParam[0]);
                newArgs.add(splitParam[1]);
            }

            // Set default values
            String x0 = reqArgs.get("x0");
            if (x0 == null) {
                x0 = "0";
            }
            String x1 = reqArgs.get("x1");
            if (x1 == null) {
                x1 = Integer.toString(Integer.parseInt(reqArgs.get("w")) - 1);
            }
            String y0 = reqArgs.get("y0");
            if (y0 == null) {
                y0 = "0";
            }
            String y1 = reqArgs.get("y1");
            if (y1 == null) {
                y1 = Integer.toString(Integer.parseInt(reqArgs.get("h")) - 1);
            }

            String requestType = "" + reqArgs.get("s") + "_" + (Integer.parseInt(x1) - Integer.parseInt(x0)) + "_" + (Integer.parseInt(y1) - Integer.parseInt(y0));

            if(sap.isDebugging()) {
                newArgs.add("-d");
            }

            // Store from ArrayList into regular String[].
            final String[] args = new String[newArgs.size()];
            int i = 0;
            for(String arg: newArgs) {
                args[i] = arg;
                i++;
            }

            // Create solver instance from factory.
            final Solver s;
            synchronized(this) {
                s = SolverFactory.getInstance().makeSolver(args);
            }

            if(s == null) {
                System.out.println("> Problem creating Solver. Exiting.");
                System.exit(1);
            }

            // Write figure file to disk.
            File responseFile = null;
            try {

                final BufferedImage outputImg = s.solveImage();

                final String outPath = WebServer.sap.getOutputDirectory();

                final String imageName = s.toString();

                final Path imagePathPNG = Paths.get(outPath, imageName);
                ImageIO.write(outputImg, "png", imagePathPNG.toFile());

                responseFile = imagePathPNG.toFile();

                Pair pair = Statistics.getStats(id);

                Map<String, AttributeValue> key = new HashMap<>();
                key.put("request", new AttributeValue(requestType));
                GetItemRequest request = new GetItemRequest(tableNameDetails, key);
                Map<String, AttributeValue> response = dynamoDB.getItem(request).getItem();
                PutItemRequest itemPut;
                if (response == null) {
                    itemPut = new PutItemRequest(tableNameDetails, newItemDetails(requestType, pair.getX().doubleValue(), pair.getY().doubleValue(), 1L));
                } else {
                    Long executions = Long.parseLong(response.get("executions").getN().toString());
                    itemPut = new PutItemRequest(tableNameDetails, newItemDetails(requestType,
                                                                                  ((Double) ((Double.parseDouble(response.get("instructions").getN().toString()) * executions) + pair.getX()) / (executions + 1)),
                                                                                  ((Double) ((Double.parseDouble(response.get("allocations").getN().toString()) * executions) + pair.getY()) / (++executions)), executions));
                }

                dynamoDB.putItem(itemPut);

                int width = Integer.parseInt(x1) - Integer.parseInt(x0);
                int height = Integer.parseInt(y1) - Integer.parseInt(y0);

                key = new HashMap<>();
                key.put("algorithm", new AttributeValue(reqArgs.get("s")));
                request = new GetItemRequest(tableNameAverage, key);
                response = dynamoDB.getItem(request).getItem();

                double average = pair.getX().doubleValue() / (width * height);
                if (response == null) {
                    itemPut = new PutItemRequest(tableNameAverage, newItemAverage(reqArgs.get("s"), average, 1L));
                } else {
                    Long executions = Long.parseLong(response.get("executions").getN().toString());
                    itemPut = new PutItemRequest(tableNameAverage, newItemAverage(reqArgs.get("s"),
                                                                                  ((Double) ((Double.parseDouble(response.get("average").getN().toString()) * executions) + average) / (++executions)),  executions));
                }

                dynamoDB.putItem(itemPut);
            } catch (final FileNotFoundException e) {
                e.printStackTrace();
            } catch (final IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Send response to browser.
            final Headers hdrs = t.getResponseHeaders();

            hdrs.add("Content-Type", "image/png");

            hdrs.add("Access-Control-Allow-Origin", "*");
            hdrs.add("Access-Control-Allow-Credentials", "true");
            hdrs.add("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS");
            hdrs.add("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

            t.sendResponseHeaders(200, responseFile.length());

            final OutputStream os = t.getResponseBody();
            Files.copy(responseFile.toPath(), os);


            os.close();

            System.out.println("> Sent response to " + t.getRemoteAddress().toString());
        }


        static Map<String, AttributeValue> newItemDetails(String request, Double instructions, Double allocations, Long executions) {
            Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
            item.put("request", new AttributeValue(request));
            item.put("instructions", new AttributeValue().withN(instructions.toString()));
            item.put("allocations", new AttributeValue().withN(allocations.toString()));
            item.put("executions", new AttributeValue().withN(executions.toString()));

            return item;
        }

        static Map<String, AttributeValue> newItemAverage(String algorithm, Double average, Long executions) {
            Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
            item.put("algorithm", new AttributeValue(algorithm));
            item.put("average", new AttributeValue().withN(average.toString()));
            item.put("executions", new AttributeValue().withN(executions.toString()));

            return item;
        }
    }
}
